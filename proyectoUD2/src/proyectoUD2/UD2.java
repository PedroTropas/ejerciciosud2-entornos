package proyectoUD2;

import java.util.Scanner;

public class UD2 {

	public static void main(String[] args) {
		String a = null;
		a=pedirPrimerCadena(a);
		if (a != null) {
			contarVocales(a);
			invertirCadena(a);
		}
	}

	// Recibe un String y lo modifica
	public static String pedirPrimerCadena(String a) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Introduce la cadena: ");
		a = sc.nextLine();
		sc.close();
		return a;
	}

	// recibe un String y cuenta el numero de vocales
	public static void contarVocales(String a) {
		int contador = 0;
		for (int x = 0; x < a.length(); x++) {
			if ((a.charAt(x) == 'a') || (a.charAt(x) == 'e') || (a.charAt(x) == 'i') || (a.charAt(x) == 'o')
					|| (a.charAt(x) == 'u')) {
				contador++;
			}
		}
		System.out.println("La palabra intoducida " + a + " tiene tantas vocales " + contador);
	}

	// dada una cadena invertirla y mostrarla por pantalla
	public static void invertirCadena(String a) {
		String b = "";
		for (int i = a.length() - 1; i >= 0; i--) {
			char invertida = a.charAt(i);
			b = b + invertida;
		}

		System.out.println("La palabra intoducida " + a + " es invertida " + b);
	}
}
